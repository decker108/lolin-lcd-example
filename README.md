# An example Lolin32 + LCD application

The pins used for this were (LCD board pins in parentheses):

* 17 (TFT_CS)
* RST (TFT_RST)
* 3V (TFT_3V)
* 21 (TFT_DC)
* 23 (TFT_MOSI)
* 18 (TFT_SCK)
* GND (TFT_G)

![Circuit board](/images/board.jpg)
